# Dart OS - Wallpapers
---

## Package:
dartos-wallpapers-all.deb

---

<div align="center">
  <table width="100%">
    <tbody>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>default.jpg</h2>
          <a href="/src/default.jpg">
            <img src="/src/default.jpg" alt="Dart OS - Wallpapers [default.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG0_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG0_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG0_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG0_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG1_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG1_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG1_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG1_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG2_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG2_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG2_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG2_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG3_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG3_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG3_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG3_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG4_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG4_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG4_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG4_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG5_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG5_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG5_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG5_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
      <tr>
        <td width="80%" style="border: none !important;">
        <div align="center" width="100%">
		  <h2>dartOS_baseBG6_by_Arijit_Bhowmick.jpg</h2>
          <a href="/src/dartOS_baseBG6_by_Arijit_Bhowmick.jpg">
            <img src="/src/dartOS_baseBG6_by_Arijit_Bhowmick.jpg" alt="Dart OS - Wallpapers [dartOS_baseBG6_by_Arijit_Bhowmick.jpg]" vertical-align="middle"/>
          </a>
        </div>
        </td>
      </tr>
    </tbody>
  <table>  
<div>

---

<hr>
<p align="center">
Developed with ❤️
</p>